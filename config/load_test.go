package config_test

import (
	"fmt"
	"os"
	"reflect"
	"testing"

	"gitlab.com/ottersoft/goapi/config"
)

type cfg struct {
	S   string
	I   int
	I8  int8
	I16 int16
	I32 int32
	I64 int64
	U   uint
	U8  uint8
	U16 uint16
	U32 uint32
	U64 uint64
	Up  uintptr
	F32 float32
	F64 float64
	B   bool
}

func TestLoadAllTypes(t *testing.T) {
	os.Setenv("S", "test")
	os.Setenv("I", "1")
	os.Setenv("I8", "2")
	os.Setenv("I16", "3")
	os.Setenv("I32", "4")
	os.Setenv("I64", "5")
	os.Setenv("U", "6")
	os.Setenv("U8", "7")
	os.Setenv("U16", "8")
	os.Setenv("U32", "9")
	os.Setenv("U64", "10")
	os.Setenv("Up", "11")
	os.Setenv("F32", "12.1")
	os.Setenv("F64", "13.2")
	os.Setenv("B", "true")

	var c cfg
	config.LoadEnvironment(&c, "S", "I", "I8", "I16", "I32", "I64", "U", "U8", "U16", "U32", "U64", "Up", "F32", "F64", "B")

	fmt.Printf("%#v\n", c)
	fmt.Printf("%#v\n", cfg{"test", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12.1, 13.2, true})
	if !reflect.DeepEqual(c, cfg{"test", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12.1, 13.2, true}) {
		t.Fail()
	}
}

func TestLoadSplitName(t *testing.T) {
	os.Setenv("testString", "foo")
	var c cfg
	config.LoadEnvironment(&c, "S=testString")
	if c.S != "foo" {
		t.Fail()
	}
}

func TestMissingField(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Fail()
		}
	}()

	os.Setenv("testString", "foo")
	var c cfg
	config.LoadEnvironment(&c, "Missing=testString")
}

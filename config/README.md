# goapi/config

YAML configuration with environment overrides.

## Getting Started

Use the `config.Load()` method to load configuration yaml files based on the environment name. The following files will be loaded from __the directory which contains the executable__, in order.

- `config.yaml`
- `config.<env>.yaml`
  - Only if env is not blank (`""`).
- `config.local.yaml`
  - Don't check this in. It's meant for local-only overrides.

```go
import (
  "os"
  "gitlab.com/ottersoft/goapi/config"
)

type MyConfig struct {
  value string
}

func main() {
  // Initialize your default configuration.
  cfg := MyConfig{
    value: "abc"
  }

  // Read and apply configuration files.
  config.Load(os.Getenv("ENVIRONMENT"), &cfg)
}
```
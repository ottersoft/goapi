package config

import (
	"os"
	"path/filepath"
)

// ExecDir returns the directory of the currently running executable.
func ExecDir() string {
	ex, err := os.Executable()

	if err != nil {
		panic(err)
	}

	a, err := filepath.Abs(filepath.Dir(ex))

	if err != nil {
		panic(err)
	}

	return a
}

package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"

	"gopkg.in/yaml.v3"
)

// Source represents information about a config file that was loaded, or
// could not be loaded due to an error (other than file-not-found).
type Source struct {
	Filename string
	Error    error
}

// LoadFile reads a YAML config file and apply it to the config struct.
func LoadFile(filename string, configPtr interface{}) (string, error) {
	a, _ := filepath.Abs(filename)
	b, err := ioutil.ReadFile(a)

	if err != nil {
		return a, err
	}

	err = yaml.Unmarshal(b, configPtr)

	return a, err
}

// LoadFrom reads YAML config files from the given directory according to the
// given environment, and applies them to the config struct.
//
// Configurations are loaded from the working directory. The following files
// will be loaded if they exist.
//
//   - config.yaml
//   - config.<env>.yaml
//   - config.local.yaml
//
// You should not check config.local.yaml into source control. It is meant for
// local-only overrides.
func LoadFrom(dir string, env string, configPtr interface{}) []Source {
	r := []Source{}

	cb := func(f string, err error) {
		r = append(r, Source{f, err})
	}

	loadOne(dir, "config.yaml", configPtr, cb)

	if env != "" {
		loadOne(dir, "config."+env+".yaml", configPtr, cb)
	}

	if env != "local" {
		loadOne(dir, "config."+env+".yaml", configPtr, cb)
	}

	return r
}

// Load reads YAML config files from the executable directory according to the
// given environment, and applies them to the config struct.
//
// Configurations are loaded from the working directory. The following files
// will be loaded if they exist.
//
//   - config.yaml
//   - config.<env>.yaml
//   - config.local.yaml
//
// You should not check config.local.yaml into source control. It is meant for
// local-only overrides.
func Load(env string, configPtr interface{}) []Source {
	return LoadFrom(ExecDir(), env, configPtr)
}

// LoadEnvironment takes the values of the named environment variables, and
// adds them to the config struct.
//
// If a named environment variable doesn't exist, then no config values will
// be modified. If the environment variable cannot be converted to the struct
// field type, then no config values will be modified.
//
// If the struct name and the environment name don't match, then you can the
// name can have the field=variable syntax, to specific the field and the
// variable name individually.
func LoadEnvironment(configPtr interface{}, names ...string) {
	t := reflect.TypeOf(configPtr)

	if t.Kind() != reflect.Ptr {
		panic(fmt.Errorf("configPtr must be a pointer"))
	}

	c := reflect.ValueOf(configPtr).Elem()

	if c.Kind() != reflect.Struct {
		panic(fmt.Errorf("configPtr must be a pointer to a struct"))
	}

	for _, n := range names {
		var v string

		if strings.Contains(n, "=") {
			a := strings.SplitN(n, "=", 2)
			n, v = a[0], a[1]
		} else {
			v = n
		}

		if s, ok := os.LookupEnv(v); ok {
			f := c.FieldByName(n)

			if !f.IsValid() {
				panic(fmt.Errorf("config field %s is not valid", n))
			} else if !f.CanSet() {
				panic(fmt.Errorf("config field %s cannot be set", n))
			} else if f.Kind() == reflect.String {
				f.SetString(s)
			} else if s == "" {
				continue
			} else if f.Kind() == reflect.Int ||
				f.Kind() == reflect.Int8 ||
				f.Kind() == reflect.Int16 ||
				f.Kind() == reflect.Int32 ||
				f.Kind() == reflect.Int64 {
				if val, err := strconv.ParseInt(s, 0, 64); err == nil {
					f.SetInt(val)
				}
			} else if f.Kind() == reflect.Uint ||
				f.Kind() == reflect.Uint8 ||
				f.Kind() == reflect.Uint16 ||
				f.Kind() == reflect.Uint32 ||
				f.Kind() == reflect.Uint64 ||
				f.Kind() == reflect.Uintptr {
				if val, err := strconv.ParseUint(s, 0, 64); err == nil {
					f.SetUint(val)
				}
			} else if f.Kind() == reflect.Float64 || f.Kind() == reflect.Float32 {
				if val, err := strconv.ParseFloat(s, 64); err == nil {
					f.SetFloat(val)
				}
			} else if f.Kind() == reflect.Bool {
				if val, err := strconv.ParseBool(s); err == nil {
					f.SetBool(val)
				}
			} else {
				panic(fmt.Errorf("config field %s is not a string, int, float, or boolean", n))
			}
		}
	}
}

func loadOne(d string, f string, c interface{}, cb func(string, error)) {
	a, err := LoadFile(filepath.Join(d, f), c)

	if err == nil || !os.IsNotExist(err) {
		cb(a, err)
	}
}

# goapi

Go API building utilities.

* [router](./router) - Use a DSL-like API to implement an `http.Handler` with path matching, filtering, and panic recovery.
* [config](./config) - YAML configuration with environment overrides.
* [pathexp](./pathexp) - Match paths and extract parameters using a string expression syntax (e.g. `/:param/:otherParams*`).
* [rule](./rule) - Pre-defined router rules (e.g. add compression, add request ID, etc.)
* [serve](./serve) - Pre-defined content serving functions (e.g. static files).

# Run the demo

Run the following command in the repo root.

```sh
go run ./examples/goapi
```

Open http://127.0.0.1:8080 in your browser.

# Contributing

This project uses [golem](https://gitlab.com/ottersoft/golem) for build scripting. Go get it, then run the `golem` command in the repo root to list the available build tasks.

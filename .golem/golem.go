package main

import (
	"fmt"

	"gitlab.com/ottersoft/golem"
	"gitlab.com/ottersoft/golem/semver"
)

func main() {
	golem.Task("build", func(t golem.Context) {
		t.RunTask("test")
		t.Exec("go", "mod", "tidy")
		t.EnsureDir("out/bin")
		t.Exec("go", "build", "-o=out/bin", "./...")
	})

	golem.Task("clean", func(t golem.Context) {
		t.Delete("**/out")
	})

	golem.Task("cover", func(t golem.Context) {
		t.RunTask("test")
		t.Exec("go", "tool", "cover", "-html=out/tmp/cp.out")
	})

	golem.Task("test", func(t golem.Context) {
		t.EnsureDir("out/tmp")
		t.Exec("go", "test", "-coverprofile", "out/tmp/cp.out", "./...")
		t.Exec("go", "tool", "cover", "-func=out/tmp/cp.out")
	})

	golem.Task("publish", func(c golem.Context) {
		c.GitAssertClean()

		var v semver.Semver
		ver := c.Option("version").String()

		if ver == "major" {
			v = c.GitVersionTags(1).Latest().BumpMajor()
		} else if ver == "minor" {
			v = c.GitVersionTags(1).Latest().BumpMinor()
		} else if ver == "" {
			v = c.GitVersionTags(1).Latest().Bump()
		} else {
			v = semver.MustParse(ver)
		}

		c.RunTask("build")
		if !c.GitChanges().Empty() {
			c.GitCommit(fmt.Sprintf("Build output for version %s", v), true)
		}

		c.Log("New version: %s", v)
		c.Publish(v)
	},
		"-version <string> Set the version to be published. Can also be 'major' or 'minor'.",
	)

	golem.Run()
}

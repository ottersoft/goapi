package pathexp_test

import (
	"net/url"
	"reflect"
	"testing"

	"gitlab.com/ottersoft/goapi/pathexp"
)

var tests = []struct {
	pat    string
	uri    string
	params url.Values
	ok     bool
}{
	{"", "", url.Values{}, true},
	{"/", "", url.Values{}, true},
	{"", "/", url.Values{}, true},
}

func TestPathexp(t *testing.T) {
	for _, tt := range tests {
		params, ok := pathexp.Compile(tt.pat).Match(tt.uri)
		if tt.ok != ok || !reflect.DeepEqual(tt.params, params) {
			t.Errorf("Compile(%#v).Match(%#v): expected (%#v, %#v), actual (%#v, %#v)",
				tt.pat, tt.uri, tt.params, tt.ok, params, ok)
		}
	}
}

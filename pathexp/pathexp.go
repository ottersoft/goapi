package pathexp

import (
	"net/url"
	"regexp"
	"strings"
)

var rxPath *regexp.Regexp
var rxUnescape *regexp.Regexp

func init() {
	rxPath = regexp.MustCompile(`(?i)(?:(:)(\w*)(?:\(((?:\\.|\\\)|[^)])*)\))?|[^:]*)`)
	rxUnescape = regexp.MustCompile(`(?:\\[\\()]|\((?:\?:)?)`)
}

// Pathexp represents a compiled path expression (e.g. "/path/:param"). The
// expression can be used to match path strings and get extracted parameters.
type Pathexp struct {
	rx    *regexp.Regexp
	names []string
}

// Match returns extracted parameters and a boolean flag indicating a positive
// or negative match result. If the boolean result is false, then the returned
// parameters will be nil.
func (p *Pathexp) Match(path string) (url.Values, bool) {
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}

	if m := p.rx.FindStringSubmatch(path); m != nil {
		params := url.Values{}

		for i, v := range m[1:] {
			params.Add(p.names[i], v)
		}

		return params, true
	}

	return nil, false
}

// Compile a path expression string.
func Compile(path string) *Pathexp {
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}

	p := "^"
	n := []string{}
	m := rxPath.FindAllStringSubmatch(path, -1)

	for _, s := range m {
		if s[1] == ":" {
			if s[2] != "" {
				n = append(n, s[2])
				p += "("
			} else {
				p += "(?:"
			}

			if s[3] == "" {
				p += "[^/]*"
			} else {
				p1 := rxUnescape.ReplaceAllStringFunc(s[3], func(s string) string {
					if s[0] == '\\' {
						s = s[1:]
					}

					if s == "(" {
						s = "(?:"
					}

					return s
				})

				p += p1
			}

			p += ")"
		} else {
			p += regexp.QuoteMeta(s[0])
		}
	}

	p += "$"
	rx := regexp.MustCompile(p)

	return &Pathexp{rx, n}
}

# goapi/pathexp

Path expressions are UNIX (forward slash only) paths which may contain literal and named segments. Expression strings can be compiled and then used to match path strings. Matching a path will generate a `url.Values`, which contains the values in the path which matched the named parameters.

A named parameter starts with a colon (`:`) followed by the parameter name. A literal segment is anything else. The parameter name can only be word characters (i.e. regular expression `\w*`). This named parameter will patch any part of a path _except_ a forward slash (i.e. regular expression `[^/]*`).

```go
exp := Pathexp.Compile(`/foo/:name`)

values, ok := exp.Match("/foo/bar")
fmt.Printf("%#v", ok) // true
fmt.Printf("%#v", values.Get("name")) // "bar"

values, ok := exp.Match("/bar")
// The literal "/foo/" prefix is missing.
fmt.Printf("%#v", ok) // false
fmt.Printf("%#v", values.Get("name")) // ""
```

A named parameter can also be followed by a regular expression in parentheses which will override the above default pattern.

```go
exp := Pathexp.Compile(`/:id(\d+)`)

values, ok := exp.Match("/123")
fmt.Printf("%#v", ok) // true
fmt.Printf("%#v", values.Get("id")) // "123"

values, ok := exp.Match("/abc")
// The "abc" segment doesn't match the regular expression: \d+
fmt.Printf("%#v", ok) // false
fmt.Printf("%#v", values.Get("id")) // ""
```

Be careful with backslashes and parentheses in named parameter regular expressions! Because the named parameter regular expression is wrapped in parentheses, all backslashes and parentheses that are part of the regular expression (not the path expression) must be escaped.

```go
exp := Pathexp.Compile(`/:param(a_\(and|or\)_b)`)
// The regular expression is `a_(and|or)_b`, with escaped parentheses.

_values_, ok := exp.Match("/a_and_b")
fmt.Printf("%#v", ok) // true
_values_, ok := exp.Match("/a_or_b")
fmt.Printf("%#v", ok) // true
_values_, ok := exp.Match("/a_not_b")
// The "not" segment doesn't match the regular expression parenthetical.
fmt.Printf("%#v", ok) // false
```

package main

import (
	"log"
	"net/http"
	"os"
	"strings"

	c "gitlab.com/ottersoft/goapi/config"
	r "gitlab.com/ottersoft/goapi/router"
	"gitlab.com/ottersoft/goapi/rule"
	s "gitlab.com/ottersoft/goapi/serve"
)

type handler struct {
	handler func(res http.ResponseWriter, req *http.Request)
}

type config struct {
	Address string `yaml:"address"`
}

func main() {
	// Default config.
	config := config{
		Address: "127.0.0.1:8080",
	}

	// Load configuration files.
	for _, f := range c.Load(os.Getenv("ENVIRONMENT"), &config) {
		if f.Error != nil {
			log.Printf(`Failed reading config file "%s": %v`, f.Filename, f.Error)
		} else {
			log.Printf(`Loaded config file "%s"`, f.Filename)
		}
	}

	router := r.NewRouter(
		// Enable debug logging (disabled by default).
		rule.SetDebugLogWriter(os.Stderr),

		// Capture or generate a request ID.
		rule.AddRequestID(),

		// Add a prefix to all log messages.
		rule.AddLogPrefix(func(req *r.Request, b *rule.LogPrefixBuilder) {
			b.Add(req.Meta(rule.KeyRequestIDs)...)
		}),

		// Write request information to the log.
		rule.AddRequestLogging(),

		// Enable response compression.
		rule.AddCompression(),

		// Recover from panics.
		r.Recover(func(res r.Response, req *r.Request, err interface{}) {
			res.ErrorLog().Printf("Internal server error: %s", err)
			res.SetStatus(http.StatusInternalServerError)
			res.Header().Add("content-type", "text/plain; charset=utf8")
			res.WriteString("Internal server error")
		}),
		r.Recover(func(res r.Response, req *r.Request, err interface{}) {
			res.DebugLog().Panic("Oh no, again!")
		}),

		// Serve content at a set of routes.
		r.Route(r.Get(r.Path("/")), func(res r.Response, req *r.Request) {
			res.WriteString("The index!")
		}),
		r.Route(r.Get(r.Path("/panic")), func(res r.Response, req *r.Request) {
			// Panic to show it will be caught by Recover rules
			res.DebugLog().Panic("Oh no!")
		}),

		// Use a resource to prefix a collection of rules.
		r.Resource("/api/",
			// Handle any of several paths.
			r.Route(r.Get(r.Path("/hello", "world")), func(res r.Response, req *r.Request) {
				res.WriteString("Hello, world!")
			}),

			// Parse path expression into parameters, and send the parameters as a
			// JSON response.
			r.Route(r.Get(r.Path(`/groups/:name/users/:id(\d+)`)), func(res r.Response, req *r.Request) {
				res.Header().Set("Content-Type", "text/plain")
				res.WriteJSON(req.Parameters())
			}),
		),

		// Serve static files. This does NOT serve directory indexes. It does set
		// the content-type header, uses the last modified time to return 304
		// responses, and supports seeking.
		r.Route(r.Get(r.Prefix("/static/")), func(res r.Response, req *r.Request) {
			fs := http.Dir(".")
			name := strings.TrimPrefix(req.URL.Path, "/static/")
			s.FileSystem(res, req, fs, name)
		}),

		// Serve a fallback if nothing else handled the request.
		r.Route(r.Any(), func(res r.Response, req *r.Request) {
			res.Log().Println("Not found:", req.URL.EscapedPath())
			res.SetStatus(http.StatusNotFound)
			res.WriteString("Not found")
		}),
	)

	log.Printf("Server starting on %s", config.Address)
	log.Fatal(http.ListenAndServe(config.Address, router))
}

package rule

import (
	u "github.com/google/uuid"
	r "gitlab.com/ottersoft/goapi/router"
)

var defaultHeaders = []string{"x-request-id", "x-correlation-id"}

// KeyRequestIDs is the unique request metadata key for Request IDs.
var KeyRequestIDs = r.NewSymbol("RequestIDs")

// AddRequestID creates a rule that finds a request ID in the request headers.
// Or, if one can't be found, it will generate a new one.
//
// The request ID is added to the request metadata under the
// filter.KeyRequestIDs key. It is also added to the response headers.
//
// If the request header was found, the same header will be set on the
// response. If no request header was found, then the response header will be
// the first header given to this function, or "x-request-id" if no headers
// were given.
func AddRequestID(headers ...string) r.RuleFunc {
	if len(headers) == 0 {
		headers = defaultHeaders
	}

	return func(res r.Response, req *r.Request, next r.ServeFunc) {
		var id string
		var name string

		for _, h := range headers {
			v := req.Header.Get(h)

			if v != "" {
				id = v
				name = h
				break
			}
		}

		if id == "" {
			id = u.New().String()
			name = headers[0]
		}

		req = req.WithMeta(KeyRequestIDs, append(req.Meta(KeyRequestIDs), id)...)
		res.Header().Set(name, id)

		next(res, req)
	}
}

package rule

import (
	"fmt"
	"strings"

	r "gitlab.com/ottersoft/goapi/router"
)

// LogPrefixFunc accepts the request and adds strings to a log prefix builder.
type LogPrefixFunc func(req *r.Request, b *LogPrefixBuilder)

// LogPrefixBuilder is a 2d array of strings, with helper methods for adding
// strings.
type LogPrefixBuilder [][]string

// Add an array of strings to the log prefix builder.
func (d *LogPrefixBuilder) Add(values ...string) {
	*d = append(*d, values)
}

// AddLogPrefix accepts a function which adds arrays of strings to a builder.
// The values added to the builder will be used to extend the current response
// logger prefix.
func AddLogPrefix(f LogPrefixFunc) r.RuleFunc {
	return func(res r.Response, req *r.Request, next r.ServeFunc) {
		var bs strings.Builder
		var bp LogPrefixBuilder

		f(req, &bp)

		for _, v := range bp {
			bs.WriteString(fmt.Sprintf("%v", v))
			bs.WriteString(" ")
		}

		if bs.Len() > 0 {
			p := bs.String()
			res.Log().SetPrefix(res.Log().Prefix() + p)
			res.ErrorLog().SetPrefix(res.ErrorLog().Prefix() + p)
			res.DebugLog().SetPrefix(res.DebugLog().Prefix() + p)
		}

		next(res, req)
	}
}

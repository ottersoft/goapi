package rule

import (
	"compress/gzip"
	"io"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	r "gitlab.com/ottersoft/goapi/router"
)

// CompressionPredicate is the predicate that is passed to the
// AddSelectiveCompression() function. The mimetype may be blank.
type CompressionPredicate func(mimeType string, size int64) bool

// AddSelectiveCompression enables dynamic GZip compression on responses.
// Responses will NOT be compressed for non-2xx status codes, or if a
// "content-encoding" response header is explicitly set.
//
// The mimetype passed to the predicate may be blank.
//
// If no content-length is explicitly set, then the length of the byte array
// to the FIRST Write() call will be used. This may not be the real length if
// Write() is called multiple times.
func AddSelectiveCompression(predicate CompressionPredicate) r.RuleFunc {
	return func(res r.Response, req *r.Request, next r.ServeFunc) {
		if !req.AcceptEncoding("gzip") {
			next(res, req)
			return
		}

		gz := gzip.NewWriter(res)
		w := &responseWriter{res, gz, predicate, false}

		defer func() {
			if w.compress && res.Written() {
				err := gz.Close()

				if err != nil {
					panic(err)
				}
			}
		}()

		res1 := r.NewResponse(w, res.Log(), res.ErrorLog(), res.DebugLog())

		next(res1, req)
	}
}

// AddCompression enables dynamic GZip compression on responses. Responses
// will NOT be compressed for non-2xx status codes, or if a "content-encoding"
// response header is explicitly set.
//
// A basic mimetype and length predicate will be used which allows  compression
// for "text-like" mimetypes that are at least 1000 bytes.
func AddCompression() r.RuleFunc {
	return AddSelectiveCompression(func(mimeType string, length int64) bool {
		if length < 1000 && length >= 0 {
			return false
		} else if match, err := regexp.MatchString("(^text/|\\+(text|json|xml)$)", mimeType); !match {
			if err != nil {
				panic(err)
			}

			return false
		}

		return true
	})
}

type responseWriter struct {
	r.ResponseWriter
	io.Writer
	predicate CompressionPredicate
	compress  bool
}

func (r *responseWriter) Write(p []byte) (int, error) {
	if !r.Written() && r.Header().Get("content-encoding") == "" && r.Status() >= 200 && r.Status() < 300 {
		if r.Header().Get("content-type") == "" {
			r.Header().Set("content-type", http.DetectContentType(p))
		}

		mt := strings.Split(r.Header().Get("content-type"), ";")[0]
		lh := r.Header().Get("content-length")
		var l int64

		if lh == "" {
			l = int64(len(p))
		} else if cl, err := strconv.ParseInt(r.Header().Get("content-length"), 10, 64); err == nil {
			l = cl
		}

		if r.predicate(mt, l) {
			r.compress = true
			r.Header().Set("content-encoding", "gzip")
			r.Header().Del("content-length")
		}
	}

	if r.compress {
		return r.Writer.Write(p)
	}

	return r.ResponseWriter.Write(p)
}

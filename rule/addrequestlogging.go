package rule

import (
	"fmt"
	"strings"
	"time"

	r "gitlab.com/ottersoft/goapi/router"
)

// AddRequestLogging creates a rule that writes request details to the log when
// the request is received, and when the request is complete.
func AddRequestLogging() r.RuleFunc {
	return func(res r.Response, req *r.Request, next r.ServeFunc) {
		start := time.Now()

		var b strings.Builder

		b.WriteString("Method=")
		b.WriteString(req.Method)
		b.WriteString(" URI=")
		b.WriteString(req.URL.RequestURI())
		b.WriteString(" Host=")
		b.WriteString(req.Host)

		s := b.String()

		res.Log().Println(s)

		defer func() {
			var b strings.Builder
			ms := float32(time.Since(start).Microseconds()) / 1000.0

			b.WriteString(s)
			b.WriteString(fmt.Sprintf(" Status=%d", res.Status()))
			b.WriteString(fmt.Sprintf(" Bytes=%d", res.ByteCount()))
			b.WriteString(fmt.Sprintf(" Time=%.6gms", ms))

			res.Log().Println(b.String())
		}()

		next(res, req)
	}
}

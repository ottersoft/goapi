package rule

import (
	"io"
	"log"

	r "gitlab.com/ottersoft/goapi/router"
)

// SetLog sets the (info) logger.
func SetLog(getLogger func(l *log.Logger) *log.Logger) r.RuleFunc {
	return func(res r.Response, req *r.Request, next r.ServeFunc) {
		res = r.NewResponse(res, getLogger(res.Log()), res.ErrorLog(), res.DebugLog())
		next(res, req)
	}
}

// SetLogWriter sets the (info) logger writer.
func SetLogWriter(w io.Writer) r.RuleFunc {
	return SetLog(func(l *log.Logger) *log.Logger {
		return log.New(w, l.Prefix(), l.Flags())
	})
}

// SetErrorLog sets the response logger.
func SetErrorLog(getLogger func(l *log.Logger) *log.Logger) r.RuleFunc {
	return func(res r.Response, req *r.Request, next r.ServeFunc) {
		res = r.NewResponse(res, res.Log(), getLogger(res.ErrorLog()), res.DebugLog())
		next(res, req)
	}
}

// SetErrorLogWriter sets the error logger writer.
func SetErrorLogWriter(w io.Writer) r.RuleFunc {
	return SetErrorLog(func(l *log.Logger) *log.Logger {
		return log.New(w, l.Prefix(), l.Flags())
	})
}

// SetDebugLog sets the response logger.
func SetDebugLog(getLogger func(l *log.Logger) *log.Logger) r.RuleFunc {
	return func(res r.Response, req *r.Request, next r.ServeFunc) {
		res = r.NewResponse(res, res.Log(), res.ErrorLog(), getLogger(res.DebugLog()))
		next(res, req)
	}
}

// SetDebugLogWriter sets the debug logger writer.
func SetDebugLogWriter(w io.Writer) r.RuleFunc {
	return SetDebugLog(func(l *log.Logger) *log.Logger {
		if w == nil {
			w = r.NilWriter
		}

		return log.New(w, l.Prefix(), l.Flags())
	})
}

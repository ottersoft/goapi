# goapi/serve

This package contains pre-defined content serving functions. These functions differ from simple response functions like `WriteJSON()` in a few ways.

- They generally require request data.
- They take complete ownership of the response and do not return bytes written or errors.

# Static Files

The following serve functions are provided for serving static files.

_None of these functions serve directory indexes!_

**Example:**

```go
import (
  "http"
  "strings"
  r "gitlab.com/ottersoft/goapi/router"
  s "gitlab.com/ottersoft/goapi/serve"
)

router := r.NewRouter(
  r.Route(r.Get(r.Prefix("/static/")), func(res r.Response, req r.Request) {
    // Create a rooted file system at the current working directory.
    fs := http.Dir(".")
    // Strip the /static/ path prefix.
    name := strings.TrimPrefix(req.URL.Path, "/static/")
    // Serve the file name from the file system.
    s.FileSystem(res, req, fs, name)
  }),
)
```

##### `FileSystem(res r.Response, req r.Request, fs http.FileSystem, name string)`

Serve a file by name, from the given file system.

A "rooted" `http.FileSystem` can be created using the `http.Dir()` utility.

```go
fs := http.Dir(".")
```

The file `name` will be normalized and all `..` parts will be removed. This means that even if the name begins with `../`, no files outside of the `fs` root directory will be served.

##### `FileSystemFiltered(res r.Response, req r.Request, fs http.FileSystem, name string, filter FileFilterFunc)`

The same as `FileSystem()`, but it accepts an additional filter function which is given the opened `http.File` and an `os.FileInfo` stats object. It can return false to prevent the file from being served.

The filter can write the status code and extra headers for the response if required. If it writes any content, then it should return false.

##### `File(res r.Response, req r.Request, f http.File)`

Serve an already opened file. _This does not close the file._

A file can be opened using the `http.FileSystem.Open()` utility.

```go
f, err := http.Dir(".").Open("index.html")

// Make sure the file gets closed.
if err != nil {
  defer f.Close()
}
```

##### `FileFiltered(res r.Response, req r.Request, f http.File, filter FileFilterFunc)`

The same as `File()`, but it accepts an additional filter function which is given the opened `http.File` and an `os.FileInfo` stats object. It can return false to prevent the file from being served.

The filter can write the status code and extra headers for the response if required. If it writes any content, then it should return false.

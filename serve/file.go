package serve

import (
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	r "gitlab.com/ottersoft/goapi/router"
)

// FileFilterFunc is the callback type for filtered file serving functions.
type FileFilterFunc func(file http.File, s os.FileInfo) bool

// FileError handles the given error. It may write an appropriate status code,
// headers, and content. Or, it may opt out of handling the response by
// invoking Pass().
func FileError(res r.Response, e interface{}) {
	err, isError := e.(error)

	if isError {
		if os.IsNotExist(err) {
			res.SetHandled(false)
			return
		} else if os.IsPermission(err) {
			res.SetStatus(http.StatusForbidden)
			return
		}
	}

	res.SetStatus(http.StatusInternalServerError)
}

// File serves an opened http.File using the http.ServeContent() mechanism.
func File(res r.Response, req *r.Request, file http.File) {
	FileFiltered(res, req, file, nil)
}

// FileFiltered serves an opened http.File, with a filter callback which can
// write the response status, headers, and optionally return false to prevent
// the file from being served.
func FileFiltered(res r.Response, req *r.Request, file http.File, filter FileFilterFunc) {
	s, err := file.Stat()

	if err != nil {
		FileError(res, err)
		return
	}

	if filter != nil {
		if !filter(file, s) {
			if !res.Written() {
				res.SetStatus(http.StatusForbidden)
			}
			return
		}
	} else {
		if s.IsDir() {
			res.SetHandled(false)
			return
		}
	}

	http.ServeContent(res.HTTPResponseWriter(), &req.Request, s.Name(), s.ModTime(), file)
}

// FileSystem opens the file on the given file system, and then serves that
// file using the File() serve function.
//
// If the accept-encoding header allows "gzip", and there is a an adjacent
// name + ".gz" file, then the compressed file will be served instead of the
// plain file.
func FileSystem(res r.Response, req *r.Request, fs http.FileSystem, name string) {
	FileSystemFiltered(res, req, fs, name, nil)
}

// FileSystemFiltered opens the file on the given file system, and then serves
// that file using the FileFiltered() serve function.
//
// If the "accept-encoding: header allows "gzip", and there is a an adjacent
// name + ".gz" file, then the compressed file will be served instead of the
// plain file.
func FileSystemFiltered(
	res r.Response,
	req *r.Request,
	fs http.FileSystem,
	name string,
	filter FileFilterFunc,
) {
	name = strings.TrimPrefix(filepath.Clean(name), "/")
	var f http.File
	var err error

	if req.AcceptEncoding("gzip") && res.Header().Get("content-encoding") == "" && !strings.HasSuffix(name, ".gz") {
		f, _ = fs.Open(name + ".gz")
	}

	if f != nil {
		res.Header().Set("content-encoding", "gzip")
		res.Header().Set("content-type", mime.TypeByExtension(filepath.Ext(name)))
	} else {
		f, err = fs.Open(name)
	}

	if err != nil {
		FileError(res, err)
		return
	}

	defer f.Close()

	FileFiltered(res, req, f, filter)
}

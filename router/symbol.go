package router

import (
	"sync/atomic"
)

var index int64 = 0

// Symbol instances are unique identifiers suitable for use as map or Context
// keys. They should be pre-defined and then reused as necessary. Two Symbols
// with the same label, WILL NOT BE EQUAL.
type Symbol struct {
	index int64
	label string
}

// String returns the symbol label.
func (m Symbol) String() string {
	return m.label
}

// NewSymbol creates a new Symbol.
func NewSymbol(label string) Symbol {
	return Symbol{atomic.AddInt64(&index, 1), label}
}

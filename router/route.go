package router

// Route creates a new Rule which calls the match function, to extract request
// parameters and determine if the serve function should be called. If the
// match function returns false, then the response will be marked unhandled.
func Route(match MatchFunc, serve ServeFunc) Rule {
	return SerialRuleFunc(func(res Response, req *Request) {
		p, ok := match(req)

		if ok {
			serve(res, req.WithParameters(p))
		} else {
			res.SetHandled(false)
		}
	})
}

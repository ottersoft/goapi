package router

// Strings is an alias for a string array, which adds array access methods.
type Strings []string

// First returns the first string in the array, or an empty string if the
// receiver is nil or empty.
func (s Strings) First() string {
	if len(s) > 0 {
		return s[0]
	}

	return ""
}

// Last returns the lat string in the array, or an empty string if the
// receiver is nil or empty.
func (s Strings) Last() string {
	if len(s) > 0 {
		return s[len(s)-1]
	}

	return ""
}

// Array casts the Strings object back to a string array ([]string).
func (s Strings) Array() []string {
	return []string(s)
}

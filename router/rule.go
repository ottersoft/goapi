package router

// Rule is an interface which defines logic for serving an HTTP response.
type Rule interface {
	// Serve handles a request. It can _wrap_ subsequent requests by calling the
	// next() callback, or allow subsequent requests to be called after it
	// returns by setting the response to unhandled.
	//
	// If the rule calls the next() callback, it should not write to the
	// response. The next() callback may also change the res.Handled() value (if
	// there are subsequent rules).
	Serve(res Response, req *Request, next ServeFunc)
}

// RuleFunc is a function which implements the Rule interface.
type RuleFunc func(res Response, req *Request, next ServeFunc)

// Serve makes RuleFunc implement the Rule interface.
func (f RuleFunc) Serve(res Response, req *Request, next ServeFunc) {
	f(res, req, next)
}

// SerialRuleFunc is a function which implements the Rule interface, but will
// never call the next() callback.
type SerialRuleFunc func(res Response, req *Request)

// Serve makes SerialRuleFunc implement the Rule interface.
func (f SerialRuleFunc) Serve(res Response, req *Request, next ServeFunc) {
	f(res, req)
}

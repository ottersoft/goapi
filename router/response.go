package router

import (
	"encoding/json"
	"fmt"
	"log"
)

// Response contains an embedded http.ResponseWriter, exposing all of its
// functionality. And, it adds additional utility functions for writing
// responses.
type Response interface {
	ResponseWriter

	// Log returns a pointer to the current logger.
	Log() *log.Logger

	// ErrorLog returns a pointer to the current error logger.
	ErrorLog() *log.Logger

	// DebugLog returns a pointer to the current debug logger.
	DebugLog() *log.Logger

	// WriteString writes the arguments as a string. The response will be written
	// using fmt.Fprintf().
	WriteString(a ...interface{}) (int, error)

	// WriteJSON writes the value as JSON text. If the Response does not yet have
	// a "content-type" header, then the header will be set to
	// "application/json".
	WriteJSON(value interface{}) (int, error)
}

type response struct {
	ResponseWriter
	log      *log.Logger
	errorLog *log.Logger
	debugLog *log.Logger
}

// NewResponse creates a new Response with the http.ResponseWriter embedded,
// and pointers to loggers.
//
// The writer can also be another instance of router.Response or
// router.ResponseWriter. In which case the new Response will have the same
// writer state (handled, written, etc.).
func NewResponse(w ResponseWriter, log *log.Logger, errorLog *log.Logger, debugLog *log.Logger) Response {
	if log == nil {
		log = CopyLogger(NilLogger)
	}

	if errorLog == nil {
		errorLog = CopyLogger(NilLogger)
	}

	if debugLog == nil {
		debugLog = CopyLogger(NilLogger)
	}

	return &response{w, log, errorLog, debugLog}
}

func (r *response) Log() *log.Logger {
	return r.log
}

func (r *response) ErrorLog() *log.Logger {
	return r.errorLog
}

func (r *response) DebugLog() *log.Logger {
	return r.debugLog
}

func (r *response) WriteString(a ...interface{}) (int, error) {
	return fmt.Fprint(r, a...)
}

func (r *response) WriteJSON(value interface{}) (int, error) {
	b, e := json.Marshal(value)

	if e != nil {
		return 0, e
	}

	if r.Header().Get("content-type") == "" {
		r.Header().Set("content-type", "application/json")
	}

	return r.Write(b)
}

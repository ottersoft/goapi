package router

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"
)

var keyParameters = NewSymbol("parameters")

// Request is an http.Request (embedded), and adds additional functionality.
// TODO: Replace this with an interface.
type Request struct {
	http.Request
}

// NewRequest creates a new Request pointer, with a shallow copy of the
// http.Request embedded.
func NewRequest(r *http.Request) *Request {
	return &Request{*r}
}

// Clone creates a new pointer to a deeply cloned Request.
func (r *Request) Clone(ctx context.Context) *Request {
	return &Request{*r.Request.Clone(ctx)}
}

// WithContext creates a new pointer to a shallow copy of the Request, with a
// new Context.
func (r *Request) WithContext(ctx context.Context) *Request {
	return &Request{*r.Request.WithContext(ctx)}
}

// WithPath creates a new pointer to a shallow copy of the Request, with an
// updated URL containing the new path.
func (r *Request) WithPath(path string) *Request {
	r1 := new(Request)
	*r1 = *r
	r1.URL = new(url.URL)
	*r1.URL = *r.URL
	r1.URL.Path = path
	return r1
}

// WithValue creates a new pointer to a shallow copy of the Request, with a
// new context that has an additional value.
func (r *Request) WithValue(key interface{}, value interface{}) *Request {
	return r.WithContext(context.WithValue(r.Context(), key, value))
}

// Value gets a value by key, from the request context.
func (r *Request) Value(key interface{}) interface{} {
	return r.Context().Value(key)
}

// WithMeta is an extension of WithValue(), which only accepts Symbol keys, and
// a Strings value.
func (r *Request) WithMeta(key Symbol, strings ...string) *Request {
	if len(strings) == 0 {
		return r
	}

	return r.WithValue(key, Strings(strings))
}

// Meta is an extension of Value(), which only accepts Symbol keys, and returns
// an Strings value, which may be nil if the key does not exist, or if the
// value associated with the key is not a Strings instance.
func (r *Request) Meta(key Symbol) Strings {
	s, _ := r.Value(key).(Strings)
	return s
}

// WithParameters creates a new pointer to a shallow copy of the request, with
// a new context, that has a url.Values map added under an internal key. The
// values are marshalled to a JSON string so that retrieving the parameters
// always returns a deep copy.
func (r *Request) WithParameters(v url.Values) *Request {
	if v == nil || len(v) == 0 {
		return r
	}

	b, err := json.Marshal(v)

	if err != nil {
		panic(err)
	}

	return r.WithValue(keyParameters, b)
}

// Parameters gets the url.Values instance which was added by the
// WithParameters() function. The returned values will be a deep copy. This may
// return nil if no parameters have been set.
func (r *Request) Parameters() (v url.Values) {
	b, _ := r.Value(keyParameters).([]byte)

	if b == nil {
		return
	}

	err := json.Unmarshal(b, &v)

	if err != nil {
		panic(err)
	}

	return
}

// AcceptEncoding returns true if the "accept-encoding" header contains the
// the given encoding.
func (r *Request) AcceptEncoding(enc string) bool {
	for _, v := range r.Header.Values("accept-encoding") {
		if strings.HasPrefix(v, enc) || strings.HasPrefix(v, "*") {
			return true
		}
	}

	return false
}

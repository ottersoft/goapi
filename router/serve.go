package router

import "net/http"

// ServeFunc is an action which handles a Request by writing the Response.
//
// A ServeFunc is used in terminal request handling cases. There is no way to
// indicate that the request has not been handled, and so the response should
// be written before the function returns.
type ServeFunc func(res Response, req *Request)

// Handler wraps a regular http.Handler as a router.ServeFunc, to support
// route handlers which weren't built for use in a Router.
func Handler(h http.Handler) ServeFunc {
	return func(res Response, req *Request) {
		h.ServeHTTP(res.HTTPResponseWriter(), &req.Request)
	}
}

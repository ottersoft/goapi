package router

import (
	"log"
	"os"
)

type nilWriter struct{}

func (w *nilWriter) Write(b []byte) (int, error) {
	// no-op
	return len(b), nil
}

// NilWriter is an io.Writer which discards all input.
var NilWriter = (*nilWriter)(nil)

// DefaultLoggerFlags are the flags used by the DefaultLogger and NilLogger.
var DefaultLoggerFlags = log.LstdFlags | log.Lmsgprefix

// DefaultLoggerWriter is the io.Writer used by the DefaultLogger.
var DefaultLoggerWriter = os.Stderr

// NilLogger discards all written data without writing it anywhere.
var NilLogger = log.New(NilWriter, "", DefaultLoggerFlags)

// DefaultLogger is the default router and request logger.
var DefaultLogger = log.New(DefaultLoggerWriter, "", DefaultLoggerFlags)

// CopyLogger returns a new logger with a writer, prefix, and flags
// copied from the given logger.
func CopyLogger(l *log.Logger) *log.Logger {
	return log.New(l.Writer(), l.Prefix(), l.Flags())
}

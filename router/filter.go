package router

// Filter rules can handle a Request by writing the Response or invoking the
// next() callback. The Request can be modified before passing it on. It
// should NOT write the Response if it calls the next() callback, unless the
// response is unhandled after the callback returns.
func Filter(f RuleFunc) Rule {
	return f
}

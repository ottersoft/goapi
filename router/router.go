package router

import (
	"log"
	"net/http"
	"regexp"
	"strings"
)

var rxSlashes = regexp.MustCompile("/{2,}")

// Router dispatches HTTP request handlers for filter, route, and recover
// rules. Rules are defined using a functional DSL-like syntax.
//
// Router implements the http.Handler interface which allows it to be passed
// to an http *Serve* function (e.g. ListenAndServe(), ListAndServeTLS(),
// Serve(), or ServeTLS()) to handle HTTP requests.
//
// Create a Router by calling the `NewRouter(rules...)` function with zero or
// more rules that define the behavior of your API.
//
// Router itself implements the Rule interface, which means a Router instance
// can be passed to the NewRouter function. The new router will have all of
// the rules of any child routers passed to it.
//
// Router will send a 500 error if a panic occurs while handling a request and
// no Recover rule handles it, and a 404 if a request doesn't match any Route
// rules.
type Router struct {
	Rule
}

// NewRouter creates a new Router instance, accepting zero or more rules to
// configure how HTTP requests will be handled.
func NewRouter(rules ...Rule) Router {
	return Router{Resource("", rules...)}
}

// ServeHTTP implements the http.Handler interface.
func (r Router) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	res := NewResponse(
		NewResponseWriter(writer),
		log.New(DefaultLogger.Writer(), "INFO  "+DefaultLogger.Prefix(), DefaultLogger.Flags()),
		log.New(DefaultLogger.Writer(), "ERROR "+DefaultLogger.Prefix(), DefaultLogger.Flags()),
		log.New(NilLogger.Writer(), "DEBUG "+NilLogger.Prefix(), NilLogger.Flags()),
	)

	defer func() {
		res.commit()
	}()
	defer func() {
		if err := recover(); err != nil {
			e, r := DecodeResponseError(err, res)
			r.ErrorLog().Printf("Panic: %s", e)

			if r.Status() < 500 {
				if r.Written() {
					r.ErrorLog().Println("Panic after response written, so 5xx status not set.")
				} else {
					r.SetStatus(http.StatusInternalServerError)
				}
			}
		}
	}()

	req := NewRequest(request)

	if !strings.HasPrefix(req.URL.Path, "/") {
		req.URL.Path = ("/" + req.URL.Path)
	}

	r.Serve(res, req, func(res Response, req *Request) {})

	if !res.Handled() {
		res.SetStatus(http.StatusNotFound)
	}
}

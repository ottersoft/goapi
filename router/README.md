# goapi/router

A router is used to define HTTP request handling.

Routers implement the `http.Handler` interface which allows them to be passed to a Go `http` *Serve* function (e.g. `ListenAndServe()`, `ListAndServeTLS()`, `Serve()`, or `ServeTLS()`) to handle HTTP requests.

Routers dispatch HTTP request handlers according to filtering, panic recovery, and response writing rules. Rules are defined using a functional DSL-like syntax. Create a router by calling the `NewRouter(rules...)` function with zero or more rules that define the behavior of your API.

The `Router` interface is itself a `Rule`, which means a router can be passed to the `NewRouter()` function. The new router will have all of the rules of any child routers passed to it.

Router will send a 500 error if a panic occurs while handling a request and no Recover rule handles it, and a 404 if a request doesn't match any Route rules.

## Getting Started

```go
import (
  r gitlab.com/ottersoft/goapi/router
)

// Create a Router.
router := r.NewRouter(
  // Recover from handler panics.
  r.Recover(func(res r.Response, req r.Request, err interface{}) {
    // Handle a panic by writing the Response, or re-panicking.
  }),

  // Filter requests.
  r.Filter(func(res r.Response, req r.Request, next ServeFunc) {
    // Do something before invoking the next handler.
    next(res, req) // Invoke the next handler (or don't).
    // Do something after invoking the next handler.
  }),

  // Match and handle requests.
  r.Route(r.Get(r.Path("/:foo")), func(res r.Response, req r.Request) {
    // Write the response.
    res.Header().Set("x-my-header", "foo")
    res.WriteHead(200)
    res.WriteJSON(req.Parameters)
  }),
)

// Use the Router to handle HTTP requests.
log.Fatal(http.ListenAndServe("127.0.0.1:8080", router))
```

# Router

##### `NewRouter(r ...Rule) Router`

Creates a new router.

# Rule Factories

##### `Filter(f FilterFunc) Rule`

Creates a rule for filtering requests.

##### `Recover(f RecoverFunc) Rule`

Creates a rule for panicking request recovery.

##### `Route(m MatchFunc, f ServeFunc) Rule`

Creates a rule for handling requests.

##### `Resource(prefix string, r ...Rule) Rule`

Creates a rule which is a collection of other rules. The inner rules are only applied when the prefix matches the beginning of the request path.

# Match Function Factories

##### `Prefix(p ...string) MatchFunc`

Creates matcher to match paths that start with one of the prefixes.

##### `Path(p ...string) MatchFunc`

Creates matcher to match one of the path expressions.

##### `Any() MatchFunc`

Creates matcher to match any request.

##### `OneOf(m ...MatchFunc) MatchFunc`

Creates matcher to match one of the matchers.

##### `Get(m ...MatchFunc) MatchFunc`

Creates matcher to match GET and one of the matchers.

##### `Head(m ...MatchFunc) MatchFunc`

Creates matcher to match HEAD and one of the matchers.

##### `Post(m ...MatchFunc) MatchFunc`

Creates matcher to match POST and one of the matchers.

##### `Put(m ...MatchFunc) MatchFunc`

Creates matcher to match PUT and one of the matchers.

##### `Delete(m ...MatchFunc) MatchFunc`

Creates matcher to match DELETE and one of the matchers.

##### `Connect(m ...MatchFunc) MatchFunc`

Creates matcher to match CONNECT and one of the matchers.

##### `Options(m ...MatchFunc) MatchFunc`

Creates matcher to match OPTIONS and one of the matchers.

##### `Trace(m ...MatchFunc) MatchFunc`

Creates matcher to match TRACE and one of the matchers.

##### `Post(m ...MatchFunc) MatchFunc`

Creates matcher to match POST and one of the matchers.

# Serve Function Factories

##### `Handler(h http.Handler) ServeFunc`

Wrap a regular `http.Handler` as a `router.ServeFunc`.

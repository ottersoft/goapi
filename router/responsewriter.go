package router

import (
	"fmt"
	"net/http"
)

// ResponseWriter is wrapper for the basic http.ResponseWriter interface.
// Instead of WriteHeader(), it has SetHeader() which does not write any
// headers, and can be called until the body is written. Headers are written
// on the first call to Write(), or when Router.ServeHTTP() returns.
type ResponseWriter interface {
	// Header returns the header map that will be sent by
	// Write. The Header map also is the mechanism with which
	// Handlers can set HTTP trailers.
	//
	// Changing the header map after a call to Write (or
	// Write) has no effect unless the modified headers are
	// trailers.
	//
	// There are two ways to set Trailers. The preferred way is to
	// predeclare in the headers which trailers you will later
	// send by setting the "Trailer" header to the names of the
	// trailer keys which will come later. In this case, those
	// keys of the Header map are treated as if they were
	// trailers. See the example. The second way, for trailer
	// keys not known to the Handler until after the first Write,
	// is to prefix the Header map keys with the TrailerPrefix
	// constant value. See TrailerPrefix.
	//
	// To suppress automatic response headers (such as "Date"), set
	// their value to nil.
	Header() http.Header

	// Write writes the data to the connection as part of an HTTP reply.
	//
	// Headers are also committed on the first call to Write().
	// If WriteHeader has not been called, the response status will be
	// http.StatusOK.
	//
	// If the Header
	// does not contain a Content-Type line, Write adds a Content-Type set
	// to the result of passing the initial 512 bytes of written data to
	// DetectContentType. Additionally, if the total size of all written
	// data is under a few KB and there are no Flush calls, the
	// Content-Length header is added automatically.
	//
	// Depending on the HTTP protocol version and the client, calling
	// Write may prevent future reads on the
	// Request.Body. For HTTP/1.x requests, handlers should read any
	// needed request body data before writing the response. Once the
	// headers have been flushed (due to either an explicit Flusher.Flush
	// call or writing enough data to trigger a flush), the request body
	// may be unavailable. For HTTP/2 requests, the Go HTTP server permits
	// handlers to continue to read the request body while concurrently
	// writing the response. However, such behavior may not be supported
	// by all HTTP/2 clients. Handlers should read before writing if
	// possible to maximize compatibility.
	Write([]byte) (int, error)

	// SetStatus sets the status code of the response. It does NOT write the
	// status or send the headers immediately (unlike
	// http.ResponseWriter.WriteHeader()). It just records the status code which
	// will be sent on the first call to Write(), or when Router.ServeHTTP()
	// returns.
	//
	// If SetStatus is not called, the response status will be
	// http.StatusOK. Thus calls to SetStatus are mainly used to
	// send error codes.
	//
	// The provided code must be a valid HTTP 1xx-5xx status code.
	// Go does not currently
	// support sending user-defined 1xx informational headers,
	// with the exception of 100-continue response header that the
	// Server sends automatically when the Request.Body is read.
	SetStatus(statusCode int)

	// Status returns the status code that was sent with SetStatus().
	Status() int

	// ByteCount returns the number of content bytes which have been written.
	ByteCount() int64

	// Written returns true if any content has been written.
	Written() bool

	// Handled returns true if the request has been handled.
	Handled() bool

	// SetHandled should be called with a false value if a handler does not
	// handle the response. The initial value is true, so explicitly setting true
	// is usually not necessary.
	SetHandled(handled bool)

	// Get an http.ResponseWriter adaptor.
	HTTPResponseWriter() http.ResponseWriter

	// commit writes status code and headers if they have not yet been written.
	commit()
}

type responseWriter struct {
	writer     http.ResponseWriter
	statusCode int
	byteCount  int64
	written    bool
	handled    bool
}

// NewResponseWriter wraps a basic http.ResponseWriter. If w is already
// wrapped, then just return it unmodified. It shouldn't be wrapped again.
func NewResponseWriter(w http.ResponseWriter) ResponseWriter {
	if w1, ok := w.(ResponseWriter); ok {
		// If w is already a ResponseWriter, then just return it.
		return w1
	}

	return &responseWriter{w, 0, 0, false, false}
}

func (w *responseWriter) Header() http.Header {
	return w.writer.Header()
}

func (w *responseWriter) Write(bytes []byte) (int, error) {
	w.commit()
	n, err := w.writer.Write(bytes)
	w.byteCount += int64(n)
	w.written = true

	return n, err
}

func (w *responseWriter) SetStatus(statusCode int) {
	if w.written {
		panic("Status code set after content written.")
	} else if statusCode < 100 || statusCode > 999 {
		panic(fmt.Sprintf("Invalid status code: %d", statusCode))
	}

	w.statusCode = statusCode
}

// WriteHeader allows ResponseWriter to be converted to an http.ResponseWriter.
func (w *responseWriter) WriteHeader(statusCode int) {
	w.SetStatus(statusCode)
}

func (w *responseWriter) Status() int {
	if w.statusCode == 0 {
		return http.StatusOK
	}
	return w.statusCode
}

func (w *responseWriter) ByteCount() int64 {
	return w.byteCount
}

func (w *responseWriter) Written() bool {
	return w.written
}

func (w *responseWriter) Handled() bool {
	return w.handled
}

func (w *responseWriter) SetHandled(handled bool) {
	if !handled && w.written {
		panic("Response marked unhandled after content written.")
	}

	w.handled = handled
}

func (w *responseWriter) HTTPResponseWriter() http.ResponseWriter {
	return http.ResponseWriter(w)
}

func (w *responseWriter) commit() {
	if !w.written {
		cleanHeaders(w.Header())

		if w.statusCode != 0 {
			w.writer.WriteHeader(w.statusCode)
		}
	}
}

func cleanHeaders(h http.Header) {
	d := make([]string, 0, len(h))

	for k, v := range h {
		if len(v) == 0 || len(v) == 1 && v[0] == "" {
			d = append(d, k)
		}
	}

	for _, k := range d {
		h.Del(k)
	}
}

package router

// RecoverFunc can either write the Response or re-panic.
type RecoverFunc func(res Response, req *Request, err interface{})

// Recover creates a rule which registers a defer/recover before calling the
// next() callback. If the next() callback panics, then the RecoverFunc
// callback will be invoked. It can either write the Response, mark the
// response as unhandled, or re-panic to allow an outer rule (or the router)
// to handle the panic.
//
// Recover handlers are handled in LIFO (last in, first out) order.
func Recover(rec RecoverFunc) Rule {
	return RuleFunc(func(res Response, req *Request, next ServeFunc) {
		defer func() {
			if err := recover(); err != nil {
				e, r := DecodeResponseError(err, res)

				defer func() {
					if err1 := recover(); err1 != nil {
						panic(NewResponseError(err1, r))
					}
				}()

				rec(r, req, e)
			}
		}()

		next(res, req)
	})
}

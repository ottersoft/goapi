package router

import (
	"strings"
	"sync/atomic"
)

type resource struct {
	prefix string
	rules  []Rule
}

// Resource rules are collections of other rules, which are only applied when
// the path starts with a prefix. It's like a sub-Router.
//
// The prefix is removed from the request path before invoking any rules.
func Resource(prefix string, rules ...Rule) Rule {
	return resource{prefix, rules}
}

// Serve implements the Rule interface, so that a Router can actually be used
// as a collection of routing rules inside another router.
func (r resource) Serve(res Response, req *Request, next ServeFunc) {
	if r.prefix == "" {
		// Always match
	} else if strings.HasPrefix(req.URL.Path, r.prefix) {
		p := req.URL.Path[len(r.prefix):]

		if !strings.HasPrefix(p, "/") {
			p = "/" + p
		}

		req = req.WithPath(p)
	} else {
		res.SetHandled(false)
		return
	}

	r.next(res, req, new(int))
}

// next begins/continues a recursive/iterative loop which drains rules to
// handle an HTTP request.
func (r resource) next(res Response, req *Request, i *int) {
	// In case there are no remaining rules (meaning the for loop will never
	// run), reset the handled state to false.
	res.SetHandled(false)
	res1 := res

	defer func() {
		if err := recover(); err != nil {
			panic(NewResponseError(err, res1))
		}
	}()

	for ; *i < len(r.rules); *i++ {
		res1 = NewResponse(res, CopyLogger(res.Log()), CopyLogger(res.ErrorLog()), CopyLogger(res.DebugLog()))
		res1.SetHandled(true)

		r.rules[*i].Serve(res1, req, r.nextCallback(i))

		if res1.Handled() {
			return
		}
	}
}

func (r resource) nextCallback(i *int) ServeFunc {
	var guard int32 = 0

	return func(res Response, req *Request) {
		if atomic.CompareAndSwapInt32(&guard, 0, 1) {
			*i++
			r.next(res, req, i)
		} else {
			panic("Router rule next() callback invoked twice by a single rule.")
		}
	}
}

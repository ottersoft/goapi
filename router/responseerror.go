package router

// ResponseError is an object which contains an Error AND the Response from
// the error context. Having the response from the error context allows
// recovery using the same Response, which maintains things log logger
// prefixes.
//
// Recover rules will decode this structure and provide the unwrapped error
// and response.
type ResponseError struct {
	Error    interface{}
	Response Response
}

// NewResponseError wraps an error into a ResponseError. If the err is already
// a ResponseError, then it will be returned unchanged.
func NewResponseError(err interface{}, res Response) ResponseError {
	e, ok := err.(ResponseError)

	if ok {
		return e
	}

	return ResponseError{err, res}
}

// DecodeResponseError unwraps the err into the error and response. If err is
// not a ResponseError, then the err will be returned unchanged and the
// default response will be returned.
func DecodeResponseError(err interface{}, defaultResponse Response) (interface{}, Response) {
	e, ok := err.(ResponseError)

	if ok {
		return e.Error, e.Response
	}

	return err, defaultResponse
}

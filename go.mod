module gitlab.com/ottersoft/goapi

go 1.14

require (
	github.com/google/uuid v1.1.1
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
